﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// VibrationManager
struct VibrationManager_t1690194139;

#include "codegen/il2cpp-codegen.h"

// System.Void VibrationManager::.ctor()
extern "C"  void VibrationManager__ctor_m3884227616 (VibrationManager_t1690194139 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void VibrationManager::Vibration()
extern "C"  void VibrationManager_Vibration_m3892457680 (VibrationManager_t1690194139 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
