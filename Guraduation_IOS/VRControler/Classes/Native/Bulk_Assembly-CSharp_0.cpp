﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <cstring>
#include <string.h>
#include <stdio.h>
#include <cmath>
#include <limits>
#include <assert.h>

// AccelManager
struct AccelManager_t3891477957;
// UnityEngine.UI.Text
struct Text_t9039225;
// System.Object
struct Il2CppObject;
// VibrationManager
struct VibrationManager_t1690194139;

#include "class-internals.h"
#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Array1146569071.h"
#include "AssemblyU2DCSharp_U3CModuleU3E86524790.h"
#include "AssemblyU2DCSharp_U3CModuleU3E86524790MethodDeclarations.h"
#include "AssemblyU2DCSharp_AccelManager3891477957.h"
#include "AssemblyU2DCSharp_AccelManager3891477957MethodDeclarations.h"
#include "mscorlib_System_Void2863195528.h"
#include "UnityEngine_UnityEngine_MonoBehaviour667441552MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Vector34282066566MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Component3501516275MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Transform1659122786MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Vector34282066566.h"
#include "UnityEngine_UnityEngine_Transform1659122786.h"
#include "mscorlib_System_String7231557.h"
#include "UnityEngine_UI_UnityEngine_UI_Text9039225.h"
#include "UnityEngine_UnityEngine_Component3501516275.h"
#include "UnityEngine_UnityEngine_Debug4195163081MethodDeclarations.h"
#include "mscorlib_System_Single4291918972MethodDeclarations.h"
#include "mscorlib_System_String7231557MethodDeclarations.h"
#include "mscorlib_System_Object4170816371.h"
#include "mscorlib_System_Single4291918972.h"
#include "UnityEngine_UI_UnityEngine_UI_Text9039225MethodDeclarations.h"
#include "mscorlib_ArrayTypes.h"
#include "UnityEngine_UnityEngine_Input4200062272MethodDeclarations.h"
#include "AssemblyU2DCSharp_VibrationManager1690194139.h"
#include "AssemblyU2DCSharp_VibrationManager1690194139MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Handheld3573483176MethodDeclarations.h"

// !!0 UnityEngine.Component::GetComponent<System.Object>()
extern "C"  Il2CppObject * Component_GetComponent_TisIl2CppObject_m267839954_gshared (Component_t3501516275 * __this, const MethodInfo* method);
#define Component_GetComponent_TisIl2CppObject_m267839954(__this, method) ((  Il2CppObject * (*) (Component_t3501516275 *, const MethodInfo*))Component_GetComponent_TisIl2CppObject_m267839954_gshared)(__this, method)
// !!0 UnityEngine.Component::GetComponent<UnityEngine.UI.Text>()
#define Component_GetComponent_TisText_t9039225_m1610753993(__this, method) ((  Text_t9039225 * (*) (Component_t3501516275 *, const MethodInfo*))Component_GetComponent_TisIl2CppObject_m267839954_gshared)(__this, method)
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void AccelManager::.ctor()
extern "C"  void AccelManager__ctor_m1571644022 (AccelManager_t3891477957 * __this, const MethodInfo* method)
{
	{
		MonoBehaviour__ctor_m2022291967(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void AccelManager::Start()
extern const MethodInfo* Component_GetComponent_TisText_t9039225_m1610753993_MethodInfo_var;
extern Il2CppCodeGenString* _stringLiteral4191987872;
extern Il2CppCodeGenString* _stringLiteral80204913;
extern const uint32_t AccelManager_Start_m518781814_MetadataUsageId;
extern "C"  void AccelManager_Start_m518781814 (AccelManager_t3891477957 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (AccelManager_Start_m518781814_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Vector3_t4282066566  L_0 = Vector3_get_zero_m2017759730(NULL /*static, unused*/, /*hidden argument*/NULL);
		__this->set_acceleration_2(L_0);
		Transform_t1659122786 * L_1 = Component_get_transform_m4257140443(__this, /*hidden argument*/NULL);
		NullCheck(L_1);
		Transform_t1659122786 * L_2 = Transform_FindChild_m2149912886(L_1, _stringLiteral4191987872, /*hidden argument*/NULL);
		NullCheck(L_2);
		Text_t9039225 * L_3 = Component_GetComponent_TisText_t9039225_m1610753993(L_2, /*hidden argument*/Component_GetComponent_TisText_t9039225_m1610753993_MethodInfo_var);
		__this->set_accelerationText_3(L_3);
		Transform_t1659122786 * L_4 = Component_get_transform_m4257140443(__this, /*hidden argument*/NULL);
		NullCheck(L_4);
		Transform_t1659122786 * L_5 = Transform_FindChild_m2149912886(L_4, _stringLiteral80204913, /*hidden argument*/NULL);
		NullCheck(L_5);
		Text_t9039225 * L_6 = Component_GetComponent_TisText_t9039225_m1610753993(L_5, /*hidden argument*/Component_GetComponent_TisText_t9039225_m1610753993_MethodInfo_var);
		__this->set_stateText_4(L_6);
		return;
	}
}
// System.Void AccelManager::UpdateScore(UnityEngine.Vector3)
extern Il2CppClass* Vector3_t4282066566_il2cpp_TypeInfo_var;
extern Il2CppClass* Debug_t4195163081_il2cpp_TypeInfo_var;
extern Il2CppClass* StringU5BU5D_t4054002952_il2cpp_TypeInfo_var;
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral3209457185;
extern Il2CppCodeGenString* _stringLiteral4191987872;
extern Il2CppCodeGenString* _stringLiteral2654190;
extern Il2CppCodeGenString* _stringLiteral2468;
extern Il2CppCodeGenString* _stringLiteral11919191;
extern Il2CppCodeGenString* _stringLiteral11948982;
extern const uint32_t AccelManager_UpdateScore_m921372252_MetadataUsageId;
extern "C"  void AccelManager_UpdateScore_m921372252 (AccelManager_t3891477957 * __this, Vector3_t4282066566  ___score0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (AccelManager_UpdateScore_m921372252_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Vector3_t4282066566  L_0 = ___score0;
		Vector3_t4282066566  L_1 = L_0;
		Il2CppObject * L_2 = Box(Vector3_t4282066566_il2cpp_TypeInfo_var, &L_1);
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t4195163081_il2cpp_TypeInfo_var);
		Debug_Log_m1731103628(NULL /*static, unused*/, L_2, /*hidden argument*/NULL);
		float L_3 = Vector3_get_sqrMagnitude_m1207423764((&___score0), /*hidden argument*/NULL);
		if ((!(((double)(((double)((double)L_3)))) < ((double)(1.5)))))
		{
			goto IL_0036;
		}
	}
	{
		Text_t9039225 * L_4 = __this->get_stateText_4();
		NullCheck(L_4);
		VirtActionInvoker1< String_t* >::Invoke(74 /* System.Void UnityEngine.UI.Text::set_text(System.String) */, L_4, _stringLiteral3209457185);
		goto IL_0046;
	}

IL_0036:
	{
		Text_t9039225 * L_5 = __this->get_stateText_4();
		NullCheck(L_5);
		VirtActionInvoker1< String_t* >::Invoke(74 /* System.Void UnityEngine.UI.Text::set_text(System.String) */, L_5, _stringLiteral4191987872);
	}

IL_0046:
	{
		Text_t9039225 * L_6 = __this->get_accelerationText_3();
		StringU5BU5D_t4054002952* L_7 = ((StringU5BU5D_t4054002952*)SZArrayNew(StringU5BU5D_t4054002952_il2cpp_TypeInfo_var, (uint32_t)6));
		NullCheck(L_7);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_7, 0);
		ArrayElementTypeCheck (L_7, _stringLiteral2654190);
		(L_7)->SetAt(static_cast<il2cpp_array_size_t>(0), (String_t*)_stringLiteral2654190);
		StringU5BU5D_t4054002952* L_8 = L_7;
		float* L_9 = (&___score0)->get_address_of_x_1();
		String_t* L_10 = Single_ToString_m639595682(L_9, _stringLiteral2468, /*hidden argument*/NULL);
		NullCheck(L_8);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_8, 1);
		ArrayElementTypeCheck (L_8, L_10);
		(L_8)->SetAt(static_cast<il2cpp_array_size_t>(1), (String_t*)L_10);
		StringU5BU5D_t4054002952* L_11 = L_8;
		NullCheck(L_11);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_11, 2);
		ArrayElementTypeCheck (L_11, _stringLiteral11919191);
		(L_11)->SetAt(static_cast<il2cpp_array_size_t>(2), (String_t*)_stringLiteral11919191);
		StringU5BU5D_t4054002952* L_12 = L_11;
		float* L_13 = (&___score0)->get_address_of_y_2();
		String_t* L_14 = Single_ToString_m639595682(L_13, _stringLiteral2468, /*hidden argument*/NULL);
		NullCheck(L_12);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_12, 3);
		ArrayElementTypeCheck (L_12, L_14);
		(L_12)->SetAt(static_cast<il2cpp_array_size_t>(3), (String_t*)L_14);
		StringU5BU5D_t4054002952* L_15 = L_12;
		NullCheck(L_15);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_15, 4);
		ArrayElementTypeCheck (L_15, _stringLiteral11948982);
		(L_15)->SetAt(static_cast<il2cpp_array_size_t>(4), (String_t*)_stringLiteral11948982);
		StringU5BU5D_t4054002952* L_16 = L_15;
		float* L_17 = (&___score0)->get_address_of_z_3();
		String_t* L_18 = Single_ToString_m639595682(L_17, _stringLiteral2468, /*hidden argument*/NULL);
		NullCheck(L_16);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_16, 5);
		ArrayElementTypeCheck (L_16, L_18);
		(L_16)->SetAt(static_cast<il2cpp_array_size_t>(5), (String_t*)L_18);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_19 = String_Concat_m21867311(NULL /*static, unused*/, L_16, /*hidden argument*/NULL);
		NullCheck(L_6);
		VirtActionInvoker1< String_t* >::Invoke(74 /* System.Void UnityEngine.UI.Text::set_text(System.String) */, L_6, L_19);
		return;
	}
}
// System.Void AccelManager::FixedUpdate()
extern Il2CppClass* Input_t4200062272_il2cpp_TypeInfo_var;
extern const uint32_t AccelManager_FixedUpdate_m46468977_MetadataUsageId;
extern "C"  void AccelManager_FixedUpdate_m46468977 (AccelManager_t3891477957 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (AccelManager_FixedUpdate_m46468977_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Input_t4200062272_il2cpp_TypeInfo_var);
		Vector3_t4282066566  L_0 = Input_get_acceleration_m3697865796(NULL /*static, unused*/, /*hidden argument*/NULL);
		__this->set_acceleration_2(L_0);
		Vector3_t4282066566  L_1 = __this->get_acceleration_2();
		AccelManager_UpdateScore_m921372252(__this, L_1, /*hidden argument*/NULL);
		return;
	}
}
// System.Void VibrationManager::.ctor()
extern "C"  void VibrationManager__ctor_m3884227616 (VibrationManager_t1690194139 * __this, const MethodInfo* method)
{
	{
		MonoBehaviour__ctor_m2022291967(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void VibrationManager::Vibration()
extern Il2CppClass* Debug_t4195163081_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral83843;
extern const uint32_t VibrationManager_Vibration_m3892457680_MetadataUsageId;
extern "C"  void VibrationManager_Vibration_m3892457680 (VibrationManager_t1690194139 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (VibrationManager_Vibration_m3892457680_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t4195163081_il2cpp_TypeInfo_var);
		Debug_Log_m1731103628(NULL /*static, unused*/, _stringLiteral83843, /*hidden argument*/NULL);
		Handheld_Vibrate_m2690509590(NULL /*static, unused*/, /*hidden argument*/NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
