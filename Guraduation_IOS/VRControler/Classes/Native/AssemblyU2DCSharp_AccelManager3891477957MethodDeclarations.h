﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// AccelManager
struct AccelManager_t3891477957;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_Vector34282066566.h"

// System.Void AccelManager::.ctor()
extern "C"  void AccelManager__ctor_m1571644022 (AccelManager_t3891477957 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AccelManager::Start()
extern "C"  void AccelManager_Start_m518781814 (AccelManager_t3891477957 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AccelManager::UpdateScore(UnityEngine.Vector3)
extern "C"  void AccelManager_UpdateScore_m921372252 (AccelManager_t3891477957 * __this, Vector3_t4282066566  ___score0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AccelManager::FixedUpdate()
extern "C"  void AccelManager_FixedUpdate_m46468977 (AccelManager_t3891477957 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
