﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UnityEngine.UI.Text
struct Text_t9039225;

#include "UnityEngine_UnityEngine_MonoBehaviour667441552.h"
#include "UnityEngine_UnityEngine_Vector34282066566.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// AccelManager
struct  AccelManager_t3891477957  : public MonoBehaviour_t667441552
{
public:
	// UnityEngine.Vector3 AccelManager::acceleration
	Vector3_t4282066566  ___acceleration_2;
	// UnityEngine.UI.Text AccelManager::accelerationText
	Text_t9039225 * ___accelerationText_3;
	// UnityEngine.UI.Text AccelManager::stateText
	Text_t9039225 * ___stateText_4;

public:
	inline static int32_t get_offset_of_acceleration_2() { return static_cast<int32_t>(offsetof(AccelManager_t3891477957, ___acceleration_2)); }
	inline Vector3_t4282066566  get_acceleration_2() const { return ___acceleration_2; }
	inline Vector3_t4282066566 * get_address_of_acceleration_2() { return &___acceleration_2; }
	inline void set_acceleration_2(Vector3_t4282066566  value)
	{
		___acceleration_2 = value;
	}

	inline static int32_t get_offset_of_accelerationText_3() { return static_cast<int32_t>(offsetof(AccelManager_t3891477957, ___accelerationText_3)); }
	inline Text_t9039225 * get_accelerationText_3() const { return ___accelerationText_3; }
	inline Text_t9039225 ** get_address_of_accelerationText_3() { return &___accelerationText_3; }
	inline void set_accelerationText_3(Text_t9039225 * value)
	{
		___accelerationText_3 = value;
		Il2CppCodeGenWriteBarrier(&___accelerationText_3, value);
	}

	inline static int32_t get_offset_of_stateText_4() { return static_cast<int32_t>(offsetof(AccelManager_t3891477957, ___stateText_4)); }
	inline Text_t9039225 * get_stateText_4() const { return ___stateText_4; }
	inline Text_t9039225 ** get_address_of_stateText_4() { return &___stateText_4; }
	inline void set_stateText_4(Text_t9039225 * value)
	{
		___stateText_4 = value;
		Il2CppCodeGenWriteBarrier(&___stateText_4, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
