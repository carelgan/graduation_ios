﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class AccelManager : MonoBehaviour 
{
	private Vector3 acceleration;
	private Text accelerationText;
	private Text stateText;

	private void Start () 
	{
		acceleration = Vector3.zero;
		accelerationText = transform.FindChild("Acceleration").GetComponent<Text>();
		stateText = transform.FindChild("State").GetComponent<Text>();
	}
	
	private void UpdateScore(Vector3 score)
	{
		Debug.Log(score);

		if(score.sqrMagnitude < 1.5)
		{
			stateText.text = "Default";
		}
		else
		{
			stateText.text = "Acceleration";
		}
	
		accelerationText.text = 
		"X : " + score.x.ToString("N2") + "\n" +
		"Y : " + score.y.ToString("N2") + "\n" +
		"Z : " + score.z.ToString("N2");
	}

	private void FixedUpdate () 
	{
		acceleration = Input.acceleration;

		UpdateScore(acceleration);
	}
}
